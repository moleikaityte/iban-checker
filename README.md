java version - 11  
open project using Intellij idea, build it and run it

check one iban:  
http://localhost:8080/api/iban/check  
  
chek a list of ibans:  
http://localhost:8080/api/iban/checkList  
  
  
data example:  
one iban:  
{  
"iban": "LT517044077788877777"  
}  
  
list of ibans:  
{  
"ibans":[  
{  
"iban": "LT787300010146887207"  
},  
{  
"iban": "LT517044077788877777"  
}  
]
}  
  
you can use swagger ui instead of postman:  
http://localhost:8080/api/swagger-ui.html  