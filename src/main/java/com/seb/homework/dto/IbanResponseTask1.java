package com.seb.homework.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class IbanResponseTask1 {
    private String iban;
    private boolean isSeb = false;
    private boolean isCorrect = false;
}
