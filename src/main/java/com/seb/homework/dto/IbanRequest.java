package com.seb.homework.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class IbanRequest {
    @NotBlank
    @Size(min = 9, max = 34)
    private String iban;
}
