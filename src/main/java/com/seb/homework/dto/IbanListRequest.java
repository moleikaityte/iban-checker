package com.seb.homework.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import java.util.List;

@Getter
@Setter
public class IbanListRequest {
    @Valid
    List<IbanRequest> ibans;
}
