package com.seb.homework.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class IbanResponseTask2 {
    private String iban;
    private boolean isCorrect = false;
    private String bank;
}
