package com.seb.homework.controller;

import com.seb.homework.dto.IbanListRequest;
import com.seb.homework.dto.IbanRequest;
import com.seb.homework.dto.IbanResponseTask1;
import com.seb.homework.dto.IbanResponseTask2;
import com.seb.homework.service.iservice.IIbanService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/iban")
public class IbanController {

    private final IIbanService ibanService;

    public IbanController(IIbanService ibanService) {
        this.ibanService = ibanService;
    }

    @PostMapping("/check")
    public @ResponseBody
    ResponseEntity<IbanResponseTask1> checkIban(@RequestBody @Valid IbanRequest ibanRequest){
        var iban = ibanService.checkIban(ibanRequest);
        if (iban == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(iban);
    }

    @PostMapping("/checkList")
    public @ResponseBody
    ResponseEntity<Set<IbanResponseTask2>> checkIban(@RequestBody @Valid IbanListRequest ibanRequests){
        var ibans = ibanService.checkIbanList(ibanRequests);
        if (ibans == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(ibans);
    }
}