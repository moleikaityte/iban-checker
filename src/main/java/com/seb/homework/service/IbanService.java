package com.seb.homework.service;

import com.seb.homework.dto.IbanListRequest;
import com.seb.homework.dto.IbanRequest;
import com.seb.homework.dto.IbanResponseTask1;
import com.seb.homework.dto.IbanResponseTask2;
import com.seb.homework.service.iservice.IIbanService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class IbanService implements IIbanService {

    @Value("${iban.length}")
    private Integer LENGTH;

    private final Integer DIVIDER = 97;
    private final Map banks = new HashMap<String, String>();

    private final ModelMapper modelMapper;

    private IbanService(){
        this.modelMapper  = new ModelMapper();

        banks.put("70440", "SEB");
        banks.put("70700", "Revolut");
        banks.put("73000", "Swedbank");
    }

    @Override
    public Set<IbanResponseTask2> checkIbanList(IbanListRequest ibanRequests){
        return ibanRequests.getIbans().stream()
                .map(ibanRequest -> {
                    var ibanResponse = modelMapper.map(checkIban(ibanRequest), IbanResponseTask2.class);
                    ibanResponse.setBank(getBankNameFromIban(ibanRequest.getIban()));

                    return ibanResponse;
                }).collect(Collectors.toSet());
    }

    @Override
    public IbanResponseTask1 checkIban(IbanRequest ibanRequest){
        var iban = ibanRequest.getIban();

        var ibanRearranged = rearrangeIban(iban);
        var ibanInteger = convertToInteger(ibanRearranged);

        var response = new IbanResponseTask1();
        response.setIban(ibanRequest.getIban());

        if (computeRemainder(ibanInteger) == 1 && isCorrectLength(iban)){
            response.setCorrect(true);
        }

        if (checkIfSeb(iban)){
            response.setSeb(true);
        }

        return response;
    }

    private BigInteger convertToInteger(String iban){
        String result = "";
        var ibanArray = iban.toCharArray();
        for (var i = 0; i < iban.length() ; i++){
            if (Character.isLetter(ibanArray[i])){
                result += ((int)ibanArray[i] - 55);
            }
            else {
                result += ibanArray[i];
            }
        }
        return new BigInteger(result);
    }

    private int computeRemainder(BigInteger iban){
        return iban.mod(new BigInteger(String.valueOf(DIVIDER))).intValue();
    }

    private boolean isCorrectLength(String iban){
        return LENGTH == iban.length();
    }

    private String rearrangeIban(String iban){
        return iban.substring(4) + iban.substring(0, 4);
    }

    private boolean checkIfSeb(String iban){
        if (iban.length() < 9){
            return false;
        }
        var bankId = iban.substring(4,9);
        return banks.get(bankId) != null;
    }

    private String getBankNameFromIban(String iban){
        if (iban.length() < 9){
            return null;
        }
        var id = iban.substring(4, 9);
        var bank = banks.get(id);

        if(bank == null){
            return null;
        }
        return bank.toString();
    }
}
