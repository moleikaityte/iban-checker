package com.seb.homework.service.iservice;

import com.seb.homework.dto.IbanListRequest;
import com.seb.homework.dto.IbanRequest;
import com.seb.homework.dto.IbanResponseTask1;
import com.seb.homework.dto.IbanResponseTask2;

import java.util.Set;

public interface IIbanService {
    Set<IbanResponseTask2> checkIbanList(IbanListRequest ibanRequests);

    IbanResponseTask1 checkIban(IbanRequest ibanRequest);
}
